package com.epam.rd.io;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

public class Part4 implements Iterable<String> {
    private String input;

    /**
     * Reads all characters from the reader.
     * @param reader A Reader
     * @throws IOException If an I/O error occurs
     */
    public Part4(Reader reader) throws IOException {
        // TODO
    }


    /**
     * Creates a Reader with given charset and passes it to <code>this(Reader)</code>
     * @param fileName A name of file
     * @param encoding A name of charset to use
     * @throws IOException If an I/O error occurs
     */
    public Part4(String fileName, String encoding) throws IOException {
        // TODO
    }

    /**
     * Create Part4 with given input
     * @param input a string to be processed
     */
    public Part4(String input) {
        // TODO
    }

    public static void main(String[] args) throws IOException {
        Part4 part = new Part4("part4.txt", "cp1251");
        for (String s : part) {
            System.out.println(s);
        }
    }

    @Override
    public Iterator<String> iterator() {
        return new IteratorImpl();
    }

    class IteratorImpl implements Iterator<String> {
        @Override
        public boolean hasNext() {
            // TODO
            return false;
        }

        @Override
        public String next() {
            // TODO
            return null;
        }
    }
}
