package com.epam.rd.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo {
    private static final InputStream STD_IN = System.in;

    public static void main(String[] args) throws IOException {
        System.out.println("=========================== PART1");
        Part1.main(args);

        System.out.println("=========================== PART2");
        Part2.main(new String[]{"-generate", "10", "0", "50", "part2.txt"});
        Part2.main(new String[]{"-sort", "part2.txt", "part2_sorted.txt"});

        System.out.println();
        System.out.println("=========================== PART3");
        try(ByteArrayInputStream in = new ByteArrayInputStream(
                "char^String^int^double^stop".replace("^", System.lineSeparator()).getBytes())) {
            // set the mock input
            System.setIn(in);
            Part3.main(new String[]{"part3.txt"});
        } finally {
            // restore the standard input
            System.setIn(STD_IN);
        }

        System.out.println("=========================== PART4");
        Part4.main(args);

        System.out.println("=========================== PART5");
        try(ByteArrayInputStream in = new ByteArrayInputStream(
                "table ru^table en^apple ru^asdf en^stop".replace("^", System.lineSeparator()).getBytes())) {
            // set the mock input
            System.setIn(in);
            Part5.main(args);
        } finally {
            // restore the standard input
            System.setIn(STD_IN);
        }

        System.out.println("=========================== PART6");
        try(ByteArrayInputStream in = new ByteArrayInputStream(
                "asdf^Latn^Cyrl^latn^cyrl^Stop".replace("^", System.lineSeparator()).getBytes())) {
            // set the mock input
            System.setIn(in);
            Part6.main(new String[]{"part6.txt", "cp1251"});
        } finally {
            // restore the standard input
            System.setIn(STD_IN);
        }
    }
}