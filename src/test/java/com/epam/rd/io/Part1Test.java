package com.epam.rd.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class Part1Test {

    private static final String UTF8_ENCODING = "UTF-8";
    private static final String CP1251_ENCODING = "cp1251";

    @ParameterizedTest
    @MethodSource("testConvertRegularData")
    void testConvertRegularData(String fName, String encoding, String expected) throws IOException {
        Part1 part = new Part1(fName, encoding);
        String text = part.convert();
        assertEquals(expected, text);
    }

    @ParameterizedTest
    @MethodSource("testConvertReadWithoutEncoding")
    void testConvertReadWithoutEncoding(String fName, String encoding, String expected) throws IOException {
        Part1 part = new Part1(fName, encoding);
        String text = part.convert();
        assertNotEquals(expected, text);
    }

    @Test
    void testConvertNullFileName() {
        assertThrows(IllegalArgumentException.class, () -> new Part1(null, UTF8_ENCODING));
    }

    @Test
    void testConvertUseDefaultEncoding() throws IOException {
        final String enc = System.getProperty("file.encoding");
        System.setProperty("file.encoding", "cp866");
        assertEquals(Utils.read("src/test/resources/part1/data3.out", "cp866"),
                new Part1("src/test/resources/part1/data3.in").convert());
        System.setProperty("file.encoding", enc);
    }

    static Stream<Arguments> testConvertRegularData() throws IOException {
        return Stream.of(
                Arguments.of("src/test/resources/part1/data1.in",
                        UTF8_ENCODING,
                        Utils.read("src/test/resources/part1/data1.out", UTF8_ENCODING)),
                Arguments.of("src/test/resources/part1/data2.in",
                        CP1251_ENCODING,
                        Utils.read("src/test/resources/part1/data2.out", CP1251_ENCODING))
        );
    }

    static Stream<Arguments> testConvertReadWithoutEncoding() throws IOException {
        return Stream.of(
                Arguments.of("src/test/resources/part1/data1.in",
                        CP1251_ENCODING,
                        Utils.read("src/test/resources/part1/data1.out", UTF8_ENCODING)),
                Arguments.of("src/test/resources/part1/data2.in",
                        UTF8_ENCODING,
                        Utils.read("src/test/resources/part1/data1.out", CP1251_ENCODING))
        );
    }
}