package com.epam.rd.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class Part4Test {

    public static final String INPUT = "Like most bank managers, Dave Calvin had acquired an " +
            "irresistible charm that he could switch on whenever he felt the necessity. " +
            "Underneath it, he was cold, calculating, brutal — a perfect murderer. I." +
            "He cooks up a plan to rob his own bank at Pittsville and make it look " +
            "as if his secretary, Alice Craig and her boyfriend, had made off with the " +
            "looted money. Alice, who is a spinster, with no parents, no relatives " +
            "and nobody to care whether she lived or died…. Dave Calvin knew just " +
            "how he could make her disappear. And to achieve his plan, he " +
            "enlisted the support of his own landlady, Kit Loring, who was sensuously " +
            "beautiful and also an alcoholic. However, Dave Calvin  was soon to realise " +
            "that a woman who is an alcoholic, isn't the most reliable partner for murder…";
    public static final String EXPECTED = "Like most bank managers, Dave Calvin had acquired an irresistible charm that he could switch on whenever he felt the necessity." +
            "Underneath it, he was cold, calculating, brutal — a perfect murderer." +
            "I." +
            "He cooks up a plan to rob his own bank at Pittsville and make it look as if his secretary, Alice Craig and her boyfriend, had made off with the looted money." +
            "Alice, who is a spinster, with no parents, no relatives and nobody to care whether she lived or died…." +
            "Dave Calvin knew just how he could make her disappear." +
            "And to achieve his plan, he enlisted the support of his own landlady, Kit Loring, who was sensuously beautiful and also an alcoholic.";
    public static final String DATA_FILE_NAME = "src/test/resources/part4/data.txt";
    static final String ENCODING1 = "UTF-8";
    static final String ENCODING2 = "cp1251";
    public static final String EXPECTED_FILE_NAME = "src/test/resources/part4/res.txt";

    @Test
    void testCodeCompliance() throws IOException {
        List<String> used = Utils.isUsed(new File("src/main/java/com/epam/rd/io/Part4.java"),
                List.of("java.util.\\w*List", "java.util.\\w*Set", "java.util.\\w*Map", "java.util.Vector"),
                true,
                null);
        assertTrue(used.isEmpty(), used.toString());
    }

    @Test
    void testIteratorBasicUsage() {
        Part4 part = new Part4(INPUT);
        StringBuilder sb = new StringBuilder();
        for (String s : part) {
            sb.append(s);
        }
        assertEquals(EXPECTED, sb.toString());
    }

    @Test
    void testIteratorBigData() throws IOException {
        Part4 part = new Part4(Files.readString(Paths.get(DATA_FILE_NAME), Charset.forName(ENCODING1)));
        StringBuilder sb = new StringBuilder();
        for (String s : part) {
            sb.append(s);
        }
        String expected = Files.readString(Paths.get(EXPECTED_FILE_NAME), Charset.forName(ENCODING2));
        assertEquals(expected, sb.toString());
    }

    @Test
    void testIteratorCorrectHasNextImpl() {
        Part4 part = new Part4(INPUT);
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = part.iterator();
        while (it.hasNext()) {
            it.hasNext();
            sb.append(it.next());
        }
        assertEquals(EXPECTED, sb.toString(),
                "Iterator#hasNext should not change internal state. " +
                "See description in Readme.md");
    }

    @Test
    void testIteratorCorrectNextImpl() {
        Part4 part = new Part4(INPUT);
        Iterator<String> it = part.iterator();
        StringBuilder sb = new StringBuilder(it.next());

        while (it.hasNext()) {
            sb.append(it.next());
        }
        assertEquals(EXPECTED, sb.toString());
    }

    @Test
    void testIteratorCorrectRemove() {
        assertThrows(UnsupportedOperationException.class, () -> new Part4(INPUT).iterator().remove(),
                "Iterator#remove must throw the exception");
    }

    @Test
    void testReaderMustBeClosed() throws IOException {
        FileReader reader = new FileReader(DATA_FILE_NAME, Charset.forName(ENCODING1));
        new Part4(reader);
        try {
            reader.read();
        } catch (IOException e) {
            assertEquals(IOException.class, e.getClass());
            assertEquals("Stream closed", e.getMessage(),
                    "You must always close resources.");
        }
    }
    @ParameterizedTest
    @MethodSource("testEncodingMustBeUsedCases")
    void testEncodingMustBeUsed(String fileName, String encoding)
            throws IOException, ReflectiveOperationException {
        Part4 part = new Part4(fileName, encoding);
        Field field = part.getClass().getDeclaredField("input");
        field.setAccessible(true);
        String actual = field.get(part).toString();
        String expected = Files.readString(Paths.get(fileName), Charset.forName(encoding));
        assertEquals(actual, expected,
                "You must use an encoding when you read files.");
    }

    static Stream<Arguments> testEncodingMustBeUsedCases() {
        return Stream.of(
                Arguments.of(DATA_FILE_NAME, ENCODING1),
                Arguments.of(EXPECTED_FILE_NAME, ENCODING2)
        );
    }
}
