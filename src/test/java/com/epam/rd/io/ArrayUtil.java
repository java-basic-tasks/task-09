package com.epam.rd.io;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayUtil {
    static boolean allElementsInRange(int[] array, int minInclusive, int maxExclusive) {
        for (int e : array) {
            if (e < minInclusive || e >= maxExclusive) {
                return false;
            }
        }
        return true;
    }

    static boolean hasElementsIn(int[] array, int... values) {
        return Arrays.stream(array).filter(a -> Arrays.stream(array).filter(v -> v == a).count() > 0).count() > 0;
    }

    static List<Integer> asList(int[] values) {
        return Arrays.stream(values).boxed().collect(Collectors.toList());
    }
}
