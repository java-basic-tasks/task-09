package com.epam.rd.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.util.stream.Stream;

import static com.epam.rd.io.Part3Test.EOL;
import static com.epam.rd.io.Part3Test.PROPERTY_FILE_ENCODING;
import static org.junit.jupiter.api.Assertions.*;

public class Part3CommandsTest {

    static final String DEFAULT_FILE_ENCODING = "cp1251";
    static final InputStream SYS_IN = System.in;
    static final PrintStream SYS_OUT = System.out;
    static final PrintStream SYS_ERR = System.err;
    static String fileEncoding;
    private ByteArrayOutputStream dataOut;
    private ByteArrayOutputStream dataErr;
    private PrintStream out;
    private PrintStream err;


    @BeforeAll
    static void beforeAll() {
        fileEncoding = System.getProperty(PROPERTY_FILE_ENCODING);
        if (fileEncoding == null) {
            fileEncoding = DEFAULT_FILE_ENCODING;
        }
        System.setProperty(PROPERTY_FILE_ENCODING, DEFAULT_FILE_ENCODING);
    }

    @AfterAll
    static void afterAll() {
        System.setIn(SYS_IN);
        System.setOut(SYS_OUT);
        System.setErr(SYS_ERR);
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
    }

    @BeforeEach
    void setUp() {
        dataOut = new ByteArrayOutputStream();
        dataErr = new ByteArrayOutputStream();
        out = new PrintStream(dataOut);
        err = new PrintStream(dataErr);
        System.setOut(out);
        System.setErr(err);
    }

    @AfterEach
    void tearDown() {
        System.setIn(SYS_IN);
        System.setOut(SYS_OUT);
        System.setErr(SYS_ERR);
        out.close();
        err.close();

    }

    @ParameterizedTest
    @MethodSource("testMainCommandCases")
    void testMainCommand(String fileName, String command, String expected) throws IOException {
        try(ByteArrayInputStream in = new ByteArrayInputStream(command.replace("#", EOL).getBytes())) {
            System.setIn(in);
            Part3.main(new String[]{fileName});
            out.flush();
            err.flush();
            assertEquals(expected, dataOut.toString(), "Command: " + command);
            assertEquals("", dataErr.toString());
        } finally {
            System.setIn(SYS_IN);
        }
    }

    static Stream<Arguments> testMainCommandCases() {
        return Stream.of(
                Arguments.of("src/test/resources/part3/part3.txt",
                        "Char#stop#", "Incorrect input" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "string#stop#", "Incorrect input" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "Integer#stop#", "Incorrect input" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "Int#stop#", "Incorrect input" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "Double#stop#", "Incorrect input" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "char#stop#", "No such values" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "String#stop#", "яd ffa bcd фвыа" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "int#stop#", "432 89" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        "double#stop#", "43.43 15. .98" + EOL
                        ),
                Arguments.of("src/test/resources/part3/part3.txt",
                        " #stop#", "Incorrect input" + EOL
                        )
        );
    }
}
