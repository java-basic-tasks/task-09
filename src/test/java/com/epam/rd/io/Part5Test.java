package com.epam.rd.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.epam.rd.io.Part3Test.EOL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Part5Test {
    private static final Logger LOG = LoggerFactory.getLogger(Part5Test.class);
    static final String RESOURCES_DIR = "target/test-classes";
    static final String RESOURCES_EN_PROPERTIES = "resources_en.properties";
    static final String RESOURCES_RU_PROPERTIES = "resources_ru.properties";
    static final InputStream SYS_IN = System.in;
    static final PrintStream SYS_OUT = System.out;
    static final PrintStream SYS_ERR = System.err;
    public static final String MSG_NO_SUCH_VALUE = "No such value";
    public static final String LOCALE_NAME_RU = "ru";
    public static final String LOCALE_NAME_EN = "en";
    private static Map<String, String> enEntries;
    private static Map<String, String> ruEntries;

    private ByteArrayOutputStream dataOut;
    private ByteArrayOutputStream dataErr;
    private PrintStream out;
    private PrintStream err;

    @BeforeAll
    static void beforeAll() throws IOException {
        LOG.trace("Start");
        Files.deleteIfExists(Paths.get(RESOURCES_DIR, RESOURCES_EN_PROPERTIES));
        Files.deleteIfExists(Paths.get(RESOURCES_DIR, RESOURCES_RU_PROPERTIES));
        List<String> keys = generateKeys(2);
        enEntries = generateValues(Utils.START_EN_INDEX, Utils.END_EN_INDEX, keys);
        LOG.debug("En enrtries: {}", enEntries);
        ruEntries = generateValues(Utils.START_CYR_INDEX, Utils.END_CYR_INDEX, keys);
        LOG.debug("Ru enrtries: {}", ruEntries);
        StringBuilder sb = new StringBuilder();
        ruEntries.entrySet().stream().map(e -> EOL + e.getKey() + " = " + unicodeEscape(e.getValue()) + EOL).forEach(sb::append);
        Path ruRes = Files.write(Paths.get(RESOURCES_DIR, RESOURCES_RU_PROPERTIES), sb.toString().getBytes(StandardCharsets.UTF_8));
        LOG.debug("Ru resources: {}", ruRes.toAbsolutePath());
        sb.setLength(0);
        enEntries.entrySet().stream().map(e -> EOL + e.getKey() + " = " + e.getValue() + EOL).forEach(sb::append);
        Path enRes = Files.writeString(Paths.get(RESOURCES_DIR, RESOURCES_EN_PROPERTIES), sb.toString(), StandardCharsets.UTF_8);
        ResourceBundle.clearCache();

        LOG.debug("En resources: {}", enRes.toAbsolutePath());
        LOG.trace("Finish");
    }

    @AfterAll
    static void afterAll() throws IOException {
        LOG.trace("Start");
        System.setIn(SYS_IN);
        System.setOut(SYS_OUT);
        System.setErr(SYS_ERR);
        boolean deleted = Files.deleteIfExists(Paths.get(RESOURCES_DIR, RESOURCES_EN_PROPERTIES));
        LOG.debug(RESOURCES_EN_PROPERTIES + " deleted: {}", deleted);
        deleted = Files.deleteIfExists(Paths.get(RESOURCES_DIR, RESOURCES_RU_PROPERTIES));
        LOG.debug(RESOURCES_RU_PROPERTIES + " deleted: {}", deleted);
        LOG.trace("Finish");
    }

    @BeforeEach
    void setUp() {
        LOG.trace("Start");
        dataOut = new ByteArrayOutputStream();
        dataErr = new ByteArrayOutputStream();
        out = new PrintStream(dataOut);
        err = new PrintStream(dataErr);
        System.setOut(out);
        System.setErr(err);
        LOG.trace("Finish");
    }

    @AfterEach
    void tearDown() {
        LOG.trace("Start");
        System.setIn(SYS_IN);
        System.setOut(SYS_OUT);
        System.setErr(SYS_ERR);
        out.close();
        err.close();
        LOG.trace("Finish");
    }

    @ParameterizedTest
    @MethodSource("testMainCases")
    void testMain(String key, String lang, String expected) throws IOException {
        LOG.trace("Start");
        LOG.debug("params: {}, {}, {}", key, lang, expected);
        try (ByteArrayInputStream dataIn = new ByteArrayInputStream(
                (key + " " + lang + EOL + "stop" + EOL).getBytes())) {
            System.setIn(dataIn);
            Part5.main(new String[]{});
            String actual = dataOut.toString();
            assertEquals(expected, actual);
            assertEquals("", dataErr.toString());
        }
        LOG.trace("Finish");
    }

    static Stream<Arguments> testMainCases() {
        LOG.trace("Start");
        Stream<Arguments> ruArgumentsStream = ruEntries.entrySet().stream()
                .map(e -> Arguments.of(e.getKey(), LOCALE_NAME_RU, e.getValue() + EOL));
        Stream<Arguments> badRuArgumentsStream = generateKeys(3).stream()
                .map(e -> Arguments.of(e, LOCALE_NAME_RU, MSG_NO_SUCH_VALUE + EOL));
        Stream<Arguments> badEnArgumentsStream = generateKeys(2).stream()
                .map(e -> Arguments.of(e, LOCALE_NAME_EN, MSG_NO_SUCH_VALUE + EOL));
        Stream<Arguments> enArgumentsStream = enEntries.entrySet().stream()
                .map(e -> Arguments.of(e.getKey(), LOCALE_NAME_EN, e.getValue() + EOL));

        Stream<Arguments> stream = Stream.concat(
                Stream.concat(Stream.concat(ruArgumentsStream, badEnArgumentsStream),
                        badRuArgumentsStream), enArgumentsStream)
                .peek(v -> LOG.debug("arguments: {}", Arrays.toString(v.get())));
        LOG.trace("Finish");
        return stream;
    }

    static List<String> generateKeys(int size) {
        return Stream.generate(() -> Utils.generateWord(3, 7, Utils.START_EN_INDEX, Utils.END_EN_INDEX))
                .limit(size)
                .collect(Collectors.toList());
    }

    static Map<String, String> generateValues(int startCharIndex, int endCharIndex, List<String> keys) {
        return keys.stream()
                .collect(Collectors.toMap(
                        s -> s,
                        v -> Utils.generateWord(3, 7, startCharIndex, endCharIndex)));
    }

    static String unicodeEscape(String text) {
        StringBuilder sb = new StringBuilder();
        for (char c : text.toCharArray()) {
            sb.append('\\').append('u').append(String.format("%04x", (int) c));
        }
        return sb.toString();
    }
}