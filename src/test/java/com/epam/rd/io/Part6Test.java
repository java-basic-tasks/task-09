package com.epam.rd.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Part6Test {
    private static final Logger LOG = LoggerFactory.getLogger(Part6Test.class);
    static final String USAGE = String.format("Usage:%n   java com.epam.rd.io.Part6 <file_name> [encoding]");
    static final String EOL = System.lineSeparator();
    static final String PROPERTY_FILE_ENCODING = "file.encoding";
    private static final byte[] commands = "japn#cyrl#Cyrl#latn#Latn#stop#".replaceAll("#", EOL).getBytes();
    public static final String CP_1251 = "cp1251";
    public static final String UTF_8 = "UTF-8";
    private static InputStream sysIn;
    private static PrintStream sysOut;
    private static PrintStream sysErr;
    private InputStream in;
    private PrintStream out;
    private PrintStream err;
    private ByteArrayOutputStream dataOut;
    private ByteArrayOutputStream dataErr;
    private static String fileEncoding;
    private static Path tempBaseDir;

    @BeforeAll
    static void beforeAll() throws IOException {
        sysIn = System.in;
        sysOut = System.out;
        sysErr = System.err;
        fileEncoding = System.getProperty(PROPERTY_FILE_ENCODING);
        tempBaseDir = Files.createTempDirectory("temp");
        Utils.generateTestData(tempBaseDir.toString());
    }

    @AfterAll
    static void afterAll() throws IOException {
        System.setIn(sysIn);
        System.setOut(sysOut);
        System.setErr(sysErr);
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
        Utils.deleteDirWithFiles(tempBaseDir);
    }

    @BeforeEach
    void setUp() {
        dataOut = new ByteArrayOutputStream();
        dataErr = new ByteArrayOutputStream();
        in = new ByteArrayInputStream(commands);
        out = new PrintStream(dataOut);
        err = new PrintStream(dataErr);
        System.setIn(in);
        System.setOut(out);
        System.setErr(err);
    }

    @AfterEach
    void tearDown() throws IOException {
        System.setIn(sysIn);
        System.setOut(sysOut);
        System.setErr(sysErr);
        in.close();
        out.close();
        err.close();

    }

    @ParameterizedTest
    @MethodSource("testMainNoFileCases")
    void testMainNoFile(Args args, String expectedRegex) {
        Part6.main(args.args);
        out.flush();
        err.flush();
        String actualOut = dataOut.toString();
        String actualErr = dataErr.toString();
        assertTrue(actualErr.matches(expectedRegex));
        assertEquals(USAGE, actualOut);
    }

    @ParameterizedTest
    @MethodSource("testMainCases")
    void testMainDefaultEncoding(String expected, String fileName, String encoding) {
        System.setProperty(PROPERTY_FILE_ENCODING, encoding);
        Part6.main(new String[]{fileName});
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
        out.flush();
        err.flush();
        assertEquals(expected, dataOut.toString());
        assertEquals("", dataErr.toString());
    }

    static Stream<Arguments> testMainNoFileCases() {
        String fileName = Utils.generateWord(5, 8, Utils.START_EN_INDEX, Utils.END_EN_INDEX);
        return Stream.of(
                Arguments.of(new Args(new String[]{}), ""),
                Arguments.of(new Args(new String[]{fileName}), "(?s)^.+" + fileName + ".*$")
        );
    }

    static Stream<Arguments> testMainCases() throws IOException {
        List<String> cyrList = Stream.generate(() -> Utils.generateWord(3, 7, Utils.START_CYR_INDEX, Utils.END_CYR_INDEX))
                .limit(5).sorted().collect(Collectors.toList());
        List<String> latList
                = Stream.generate(() -> Utils.generateWord(3, 7, Utils.START_EN_INDEX, Utils.END_EN_INDEX))
                .limit(5).sorted().collect(Collectors.toList());
        List<String> shuffled = new ArrayList<>(cyrList);
        shuffled.addAll(latList);
        String cyrText = String.join(" ", cyrList);
        LOG.debug("CyrText: {}", cyrText);
        String latText = String.join(" ", latList);
        LOG.debug("LatText: {}", latText);
        String text = String.join(" ", shuffled);
        String winTempPath = createTempFile(text, CP_1251).toString();
        String nixTempPath = createTempFile(text, UTF_8).toString();
        return Stream.of(
                Arguments.of(
                        "japn: Incorrect input" + EOL +
                                "cyrl: " + cyrText + EOL +
                                "Cyrl: " + cyrText + EOL +
                                "latn: " + latText + EOL +
                                "Latn: " + latText + EOL,
                        winTempPath, CP_1251),
                Arguments.of(
                        "japn: Incorrect input" + EOL +
                                "cyrl: " + cyrText + EOL +
                                "Cyrl: " + cyrText + EOL +
                                "latn: " + latText + EOL +
                                "Latn: " + latText + EOL,
                        nixTempPath, UTF_8)
        );
    }

    private static Path createTempFile(String text, String encoding) throws IOException {
        Path tempFile = Files.createTempFile(tempBaseDir, "", "");
        Files.writeString(tempFile, text, Charset.forName(encoding), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
        return tempFile;
    }

}
