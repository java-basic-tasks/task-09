package com.epam.rd.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class Part2MainTest {
    static final String USAGE = String.format("Usage: %n" +
            "   java com.epam.rd.io.Part2 -generate <size> <min_bound_inclusive> <max_bound_exclusive> <name_of_file>%n" +
            "   java com.epam.rd.io.Part2 -sort name_of_input_file name_of output_file");
    private static final String EOL = System.lineSeparator();
    static PrintStream sysOut = System.out;
    static PrintStream sysErr = System.err;
    PrintStream out;
    PrintStream err;
    private ByteArrayOutputStream dataOut;
    private ByteArrayOutputStream dataErr;
    static Path tempDir;

    @BeforeAll
    static void beforeAll() throws IOException {
        sysOut = System.out;
        sysErr = System.err;
        tempDir = Files.createTempDirectory("temp");
    }

    @AfterAll
    static void afterAll() throws IOException {
        System.setOut(sysOut);
        System.setErr(sysErr);
        Files.deleteIfExists(tempDir);
    }

    @BeforeEach
    void setUp() {
        dataOut = new ByteArrayOutputStream();
        dataErr = new ByteArrayOutputStream();
        out = new PrintStream(dataOut);
        err = new PrintStream(dataErr);
        System.setOut(out);
        System.setErr(err);
    }

    @AfterEach
    void tierDown() {
        out.close();
        err.close();
        cleanUpFiles();
    }

    @ParameterizedTest
    @MethodSource("testMainWrongParametersCases")
    void testMainWrongParameters(Args arguments) {
        Part2.main(arguments.args);
        assertEquals(USAGE, dataOut.toString());
        String actual = dataErr.toString();
        assertNotEquals("null", actual);
        assertTrue(actual.length() > 0 && !actual.isBlank());
    }

    @ParameterizedTest
    @MethodSource("testMainSortShouldReturnCorrectResultsCases")
    void testMainSortShouldReturnCorrectResults(Args arguments, String expected) throws IOException {
        Part2.main(arguments.args);
        String actual = dataOut.toString();
        assertEquals(expected, actual);
        assertEquals("", dataErr.toString());
        assertTrue(Files.exists(Path.of(arguments.args[2])));
        String output = "output ==> ";
        assertEquals(actual.substring(actual.indexOf(output) + output.length()), Files.readString(Paths.get(arguments.args[2])));
    }

    @ParameterizedTest
    @MethodSource("testMainGenerateShouldReturnCorrectResultsCases")
    void testMainGenerateShouldReturnCorrectResults(Args arguments) throws IOException {
        Part2.main(arguments.args);
        String arg = arguments.args[4];
        String actualData = Files.readString(Paths.get(arg));
        assertEquals(actualData.trim(), actualData);
        assertEquals(Integer.parseInt(arguments.args[1]), actualData.trim().split(" ").length);
        String actualOut = dataOut.toString();
        String actualErr = dataErr.toString();
        assertEquals("", actualOut);
        assertEquals("", actualErr);
    }

    static Stream<Arguments> testMainWrongParametersCases() {
        return Stream.of(
                Arguments.of(new Args(new String[]{})),
                Arguments.of(new Args(new String[]{"-generate"})),
                Arguments.of(new Args(new String[]{"-generate", "5"})),
                Arguments.of(new Args(new String[]{"-generate", "5", "-2"})),
                Arguments.of(new Args(new String[]{"-generate", "0", "-2", "3"})),
                Arguments.of(new Args(new String[]{"-g", "0", "--2", "3", tempDir + "/out.txt"})),
                Arguments.of(new Args(new String[]{"-sort"})),
                Arguments.of(new Args(new String[]{"-sort", tempDir + "/data.txt"})),
                Arguments.of(new Args(new String[]{"-srt", tempDir + "/data.txt"}))
        );
    }

    static Stream<Arguments> testMainSortShouldReturnCorrectResultsCases() {
        return Stream.of(
                Arguments.of(new Args(new String[]{"-sort", "src/test/resources/part2/in1.txt", tempDir + "/out1.txt"}),
                        "input ==> 1 2 2 2 3 3 0 -1 0 4" + EOL +
                                "output ==> -1 0 0 1 2 2 2 3 3 4"),
                Arguments.of(new Args(new String[]{"-sort", "src/test/resources/part2/in2.txt", tempDir + "/out2.txt"}),
                        "input ==> 0 2" + EOL +
                                "output ==> 0 2"),
                Arguments.of(new Args(new String[]{"-sort", "src/test/resources/part2/in3.txt", tempDir + "/out3.txt"}),
                        "input ==> -19 -18 -6 4 7 8 -3 -5 -20 -11 -5 -19 1 -1 -10 -11 -10 -10 -16 0 6 -17 2 -15 11 -20 -10 1 -10 12 19 -12 14 -14 -7 7 16 19 -4 11 17 18 -17 -3 -3 -12 -13 11 -15 3 10 0 -3 2 13 5 -3 18 6 10 17 1 -20 0 -18 -8 11 -20 19 -9 10 -5 -17 -5 -10 -15 19 -7 13 3 8 7 4 9 -16 -7 -16 16 7 2 -2 -1 -8 15 -9 2 -18 11 5 4" + EOL +
                                "output ==> -20 -20 -20 -20 -19 -19 -18 -18 -18 -17 -17 -17 -16 -16 -16 -15 -15 -15 -14 -13 -12 -12 -11 -11 -10 -10 -10 -10 -10 -10 -9 -9 -8 -8 -7 -7 -7 -6 -5 -5 -5 -5 -4 -3 -3 -3 -3 -3 -2 -1 -1 0 0 0 1 1 1 2 2 2 2 3 3 4 4 4 5 5 6 6 7 7 7 7 8 8 9 10 10 10 11 11 11 11 11 12 13 13 14 15 16 16 17 17 18 18 19 19 19 19"),
                Arguments.of(new Args(new String[]{"-sort", "src/test/resources/part2/in4.txt", tempDir + "/out4.txt"}),
                        "input ==> 3 1 4 4 0 2 2 2 1 4" + EOL +
                                "output ==> 0 1 1 2 2 2 3 4 4 4")
        );
    }

    static Stream<Arguments> testMainGenerateShouldReturnCorrectResultsCases() {
        return Stream.of(
                Arguments.of(new Args(new String[]{"-generate", "10", "-2", "5", tempDir + "/in1.txt"})),
                Arguments.of(new Args(new String[]{"-generate", "2", "0", "7", tempDir + "/in2.txt"})),
                Arguments.of(new Args(new String[]{"-generate", "100", "-20", "20", tempDir + "/in3.txt"})),
                Arguments.of(new Args(new String[]{"-generate", "10", "0", "5", tempDir + "/in4.txt"}))
        );
    }

    private void cleanUpFiles() {
        try {
            Files.list(tempDir).forEach((p) -> {
                try {
                    Files.deleteIfExists(p);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}