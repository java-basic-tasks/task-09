package com.epam.rd.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.opentest4j.AssertionFailedError;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class DemoTest {

    private static final int TIMEOUT = 5 * 1000;
    public static final InputStream STD_IN = System.in;

    @Test
    void testMainShouldMockSystemIn() throws InterruptedException {
        ByteArrayInputStream mock = Mockito.mock(ByteArrayInputStream.class);
        Mockito.when(mock.read()).thenReturn(-1);
        System.setIn(mock);
        Thread t = new Thread(() -> {
            try {
                Demo.main(new String[]{});
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        t.setDaemon(true);
        t.start();
        while (t.isAlive()) {
            t.join(1000);
            Mockito.verifyNoInteractions(mock);
        }
        System.setIn(STD_IN);
    }
}