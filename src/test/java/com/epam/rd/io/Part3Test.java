package com.epam.rd.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Part3Test {

    static final String USAGE = String.format("Usage:%n   java com.epam.rd.io.Part3 <file_name>");
    static final String EOL = System.lineSeparator();
    static final String PROPERTY_FILE_ENCODING = "file.encoding";
    private static final byte[] commands = "char#String#int#double#stop#".replaceAll("#", EOL).getBytes();
    private static InputStream sysIn;
    private static PrintStream sysOut;
    private static PrintStream sysErr;
    private InputStream in;
    private PrintStream out;
    private PrintStream err;
    private ByteArrayOutputStream dataOut;
    private ByteArrayOutputStream dataErr;
    private static String fileEncoding;
    private static Path tempBaseDir;

    @BeforeAll
    static void beforeAll() throws IOException {
        sysIn = System.in;
        sysOut = System.out;
        sysErr = System.err;
        fileEncoding = System.getProperty(PROPERTY_FILE_ENCODING);
        tempBaseDir = Files.createTempDirectory("temp");
        Utils.generateTestData(tempBaseDir.toString());
    }

    @AfterAll
    static void afterAll() throws IOException {
        System.setIn(sysIn);
        System.setOut(sysOut);
        System.setErr(sysErr);
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
        Utils.deleteDirWithFiles(tempBaseDir);
    }

    @BeforeEach
    void setUp() {
        dataOut = new ByteArrayOutputStream();
        dataErr = new ByteArrayOutputStream();
        in = new ByteArrayInputStream(commands);
        out = new PrintStream(dataOut);
        err = new PrintStream(dataErr);
        System.setIn(in);
        System.setOut(out);
        System.setErr(err);
    }

    @AfterEach
    void tearDown() throws IOException {
        System.setIn(sysIn);
        System.setOut(sysOut);
        System.setErr(sysErr);
        in.close();
        out.close();
        err.close();

    }

    @ParameterizedTest
    @MethodSource("testMainNoFileCases")
    void testMainNoFile(Args args, String expectedRegex) {
        Part3.main(args.args);
        out.flush();
        err.flush();
        String actualOut = dataOut.toString();
        String actualErr = dataErr.toString();
        assertTrue(actualErr.matches(expectedRegex));
        assertEquals(USAGE, actualOut);
    }

    @ParameterizedTest
    @MethodSource("testMainOneTypeCases")
    void testMainOneType(String expected, String fileName, String encoding) {
        System.setProperty(PROPERTY_FILE_ENCODING, encoding);
        Part3.main(new String[]{fileName});
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
        out.flush();
        err.flush();
        assertEquals(expected, dataOut.toString());
        assertEquals("", dataErr.toString());
    }

    @ParameterizedTest
    @MethodSource("testMainAllTypesCases")
    void testMainAllTypes(String expected, String fileName, String encoding) {
        System.setProperty(PROPERTY_FILE_ENCODING, encoding);
        Part3.main(new String[]{fileName});
        System.setProperty(PROPERTY_FILE_ENCODING, fileEncoding);
        out.flush();
        err.flush();
        String actualOut = dataOut.toString();
        String actualErr = dataErr.toString();
        Scanner sc = new Scanner(new StringReader(actualOut));
        StringBuilder sb = new StringBuilder();
        while (sc.hasNextLine()) {
            sb.append(sort(sc.nextLine())).append(EOL);
        }
        assertEquals(expected, sb.toString());
        assertEquals("", actualErr);
    }


    static Stream<Arguments> testMainNoFileCases() {
        String regex = "^\\S+.+" + EOL + "$";
        return Stream.of(
                Arguments.of(new Args(new String[]{}), regex),
                Arguments.of(new Args(new String[]{Utils.generateChars(5, 0, 52).toString().replaceAll("[\\[\\], ]", "")}),
                        regex)
        );
    }

    static Stream<Arguments> testMainOneTypeCases() throws IOException {
        return Stream.of(
                Arguments.of(
                        Files.readString(Paths.get(tempBaseDir + "/chars_cp1251.txt"), Charset.forName("cp1251")) + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL,
                        tempBaseDir + "/chars_cp1251.txt", "cp1251"),
                Arguments.of(
                        Files.readString(Paths.get(tempBaseDir + "/chars_utf-8.txt"), StandardCharsets.UTF_8) + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL,
                        tempBaseDir + "/chars_utf-8.txt", "UTF-8"),
                Arguments.of(
                        "No such values" + EOL +
                                Files.readString(Paths.get(tempBaseDir + "/strings_cp1251.txt"), Charset.forName("cp1251")) + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL,
                        tempBaseDir + "/strings_cp1251.txt", "cp1251"),
                Arguments.of(
                        "No such values" + EOL +
                                Files.readString(Paths.get(tempBaseDir + "/strings_utf-8.txt"), StandardCharsets.UTF_8) + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL,
                        tempBaseDir + "/strings_utf-8.txt", "UTF-8"),
                Arguments.of(
                        "No such values" + EOL +
                                "No such values" + EOL +
                                Files.readString(Paths.get(tempBaseDir + "/ints.txt"), StandardCharsets.UTF_8) + EOL +
                                "No such values" + EOL,
                        tempBaseDir + "/ints.txt", "UTF-8"),
                Arguments.of(
                        "No such values" + EOL +
                                "No such values" + EOL +
                                "No such values" + EOL +
                                Files.readString(Paths.get(tempBaseDir + "/doubles.txt"), StandardCharsets.UTF_8) + EOL,
                        tempBaseDir + "/doubles.txt", "UTF-8")
        );
    }

    static Stream<Arguments> testMainAllTypesCases() throws IOException {
        return Stream.of(
                Arguments.of(
                        sort(Files.readString(Paths.get(tempBaseDir + "/chars_utf-8.txt"), StandardCharsets.UTF_8)) + EOL +
                                sort(Files.readString(Paths.get(tempBaseDir + "/strings_utf-8.txt"), StandardCharsets.UTF_8)) + EOL +
                                sort(Files.readString(Paths.get(tempBaseDir + "/ints.txt"), StandardCharsets.UTF_8)) + EOL +
                                sort(Files.readString(Paths.get(tempBaseDir + "/doubles.txt"), StandardCharsets.UTF_8)) + EOL,
                        tempBaseDir + "/allinone.txt", "UTF-8")
        );
    }

    static String sort(String text) {
        return Arrays.stream(text.split(" "))
                .sorted().collect(Collectors.joining(" "));
    }
}
