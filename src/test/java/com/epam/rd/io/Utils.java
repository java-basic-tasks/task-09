package com.epam.rd.io;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Utils {
    static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "abcdefghijklmnopqrstuvwxyz" +
            "АБВГҐДЕЁЖЗИЙІЇКЛМНОПРСТУФХЦЧШЩЪЫЬЭЄЮЯ" +
            "абвгґдеёжзийіїклмнопрстуфхцчшщъыьэєюя";

    static final int START_EN_INDEX = CHARS.indexOf('A');
    static final int END_EN_INDEX = CHARS.indexOf('z');
    static final int START_CYR_INDEX = CHARS.indexOf('А');
    static final int END_CYR_INDEX = CHARS.indexOf('я');

    public static String read(String fName, String encoding) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fName)), encoding);
    }

    static void deleteDirWithFiles(Path dir) throws IOException {
        Files.list(dir).forEach(path -> {
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Files.deleteIfExists(dir);
    }

    static List<String> isUsed(File path, List<String> decline, boolean acceptSubPackages, List<String> accept) throws IOException {
        List<String> list = getUsed(path, decline);
        if (acceptSubPackages) {
            acceptSubPackages(decline, list);
        }
        if (!(accept == null || accept.isEmpty())) {
            list.removeAll(accept);
        }
        return list;
    }

    static void acceptSubPackages(List<String> decline, List<String> from) {
        for (String d : decline) {
            String regex = d.endsWith("*") ?
                    d.substring(0, d.length() - 1) + "\\w+\\.[\\w.]+" :
                    "^" + d + "\\w+\\.[\\w.]+";
            from.removeIf(s -> s.matches(regex));
        }
    }

    static List<String> getUsed(File path, List<String> decline) throws IOException {
        String source = Files.readString(Paths.get(path.toString())).replaceAll("[\r\n]", " ");
        String regex = getRegex(decline);
        Matcher m = Pattern.compile(regex).matcher(source);
        List<String> list = new ArrayList<>();

        while (m.find()) {
            list.add(m.group().replace("\\s*", ""));
        }
        System.out.println(list);
        return list;
    }

    private static String getRegex(List<String> list) {
        return list.stream()
                .map(v -> v.replace(".", "\\s*\\.\\s*"))
                .map(v -> v.endsWith("*") ? v.substring(0, v.lastIndexOf("*")) + "[\\w\\.]+" : v)
                .collect(Collectors.joining("|"));
    }

    static int nextRandomValue(Random r, int min, int max) {
        return r.nextInt(max - min) + min;
    }

    static List<String> generateIntegers(int size, int minInclusive, int maxExclusive) {
        Random r = new Random();
        return Stream.generate(() -> nextRandomValue(r, minInclusive, maxExclusive))
                .limit(size)
                .map(String::valueOf).collect(toList());
    }

    static List<String> generateDoubles(int size, int minInclusive, int maxExclusive) {
        Random r = new Random();
        return Stream.generate(() -> r.nextDouble() * maxExclusive + minInclusive)
                .limit(size)
                .map(d -> String.format("%2.2f", d))
                .map(Utils::doubleVariations)
                .collect(toList());
    }

    static List<String> generateChars(int size, int minValue, int maxValue) {
        Random r = new Random();
        return Stream.generate(() -> nextRandomValue(r, minValue, maxValue))
                .limit(size).map(i -> String.valueOf(CHARS.charAt(i))).collect(toList());
    }

    static List<String> generateWords(int size, int minLength, int maxLength) {
        return IntStream.range(0, size)
                .boxed()
                .map(n -> generateWord(minLength, maxLength))
                .collect(toList());
    }

    static String generateWord(int minLength, int maxLength) {
        return generateWord(minLength, maxLength, START_EN_INDEX, END_CYR_INDEX);
    }

    static String generateWord(int minLength, int maxLength, int start, int end) {
        Random r = new Random();
        return Stream.generate(() -> nextRandomValue(r, start, end + 1))
                .limit(nextRandomValue(r, minLength, maxLength))
                .map(CHARS::charAt).map(String::valueOf)
                .collect(joining(""));
    }

    static List<String> shuffle(List<String> list) {
        List<Integer> indexes = generateUniqueSequence(list.size(), 0, list.size());
        return indexes.stream()
                .map(list::get)
                .collect(toList());
    }

    static List<Integer> generateUniqueSequence(int size, int minBound, int maxBound) {
        List<Integer> values = new ArrayList<>();
        Random r = new Random();
        Stream.generate(() -> nextRandomValue(r, minBound, maxBound))
                .filter(v -> !values.contains(v))
                .limit(size)
                .forEach(values::add);
        return values;
    }

    static String doubleVariations(String s) {
        Random r = new Random();
        int v = r.nextInt();
        int endIndex = s.indexOf('.');
        return v % 3 == 0 ? s :
                v % 3 == 1 ? s.substring(0, endIndex + 1) : s.substring(endIndex);
    }

    interface StringsGenerator {
        List<String> generate(int size, int min, int max);
    }

    public static void main(String[] args) throws IOException {
        generateTestData("src/test/resources/part3");
    }

    public static void generateTestData(String baseDir) throws IOException {
        List<String> stringList = generateWords(50, 2, 8);
        String strings = String.join(" ", stringList);
        Files.write(Paths.get(baseDir + "/strings_cp1251.txt"),
                strings.getBytes(Charset.forName("cp1251")),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(Paths.get(baseDir + "/strings_utf-8.txt"),
                strings.getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        List<String> charsList = generateChars(50, 0, 26 * 2 + 35 * 2);
        String chars = String.join(" ", charsList);
        Files.write(Paths.get(baseDir + "/chars_cp1251.txt"),
                chars.getBytes(Charset.forName("cp1251")),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(Paths.get(baseDir + "/chars_utf-8.txt"),
                chars.getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        List<String> integersList = generateIntegers(50, 0, 26 * 2 + 35 * 2);
        String ints = String.join(" ", integersList);
        Files.write(Paths.get(baseDir + "/ints.txt"),
                ints.getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        Locale defaultLocale = Locale.getDefault();
        Locale.setDefault(Locale.US);

        List<String> doublesList = generateDoubles(50, 0, 26 * 2 + 35 * 2);
        String doubles = String.join(" ", doublesList);
        Files.write(Paths.get(baseDir + "/doubles.txt"),
                doubles.getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        Locale.setDefault(defaultLocale);
        List<String> all = new ArrayList<>(stringList);
        all.addAll(charsList);
        all.addAll(integersList);
        all.addAll(doublesList);
        List<String> shuffled = shuffle(all);
        String allInOne = String.join(" ", shuffled);
        Files.write(Paths.get(baseDir + "/allinone.txt"),
                allInOne.getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }
}
