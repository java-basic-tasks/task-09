
package com.epam.rd.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DemoInternalTest {
    public static final InputStream STD_IN = System.in;

    static class MockInputStream extends InputStream {

        @Override
        public synchronized int read() {
            throw new UnsupportedOperationException("Should not be used");
        }
    }

    @Test
    void testMainShouldMockSystemIn() throws IOException {
        try (MockInputStream mock = new MockInputStream()) {
            System.setIn(mock);
            Assertions.assertDoesNotThrow(() -> Demo.main(new String[]{}));
        } finally {
            System.setIn(STD_IN);
        }
    }
}