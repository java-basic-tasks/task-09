package com.epam.rd.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class Part2Test {

    @Test
    void testCompliance() throws IOException {
        List<String> shouldNotBeUsed = List.of("java.util.*");
        List<String> canBeUsed = List.of("java.util.Random");
        List<String> actual = Utils.isUsed(new File("src/main/java/com/epam/rd/io/Part2.java"),
                shouldNotBeUsed, false, canBeUsed);
        assertTrue(actual.isEmpty(),
                "java.util package is forbidden. " +
                        "You can use only subpackages and " + canBeUsed);
    }

    @ParameterizedTest
    @MethodSource("testGenerateCases")
    void testGenerate(int size, int minBound, int maxBound) {
        int[] generated = Part2.generate(size, minBound, maxBound);
        assertEquals(size, generated.length);
        assertTrue(ArrayUtil.allElementsInRange(generated, minBound, maxBound));
        assertTrue(ArrayUtil.hasElementsIn(generated, minBound, maxBound));
    }

    @ParameterizedTest
    @MethodSource("testIntArrayToStringCases")
    void testIntArrayToString(int[] values, String separator, String expected) {
        assertEquals(expected, Part2.intArrayToString(values, separator));
    }

    @ParameterizedTest
    @MethodSource("testStringToIntArrayShouldThrowNFECases")
    void testStringToIntArrayShouldThrowNFE(String text, String regex) {
        assertThrows(NumberFormatException.class, () -> Part2.stringToIntArray(text, regex));
    }

    @ParameterizedTest
    @MethodSource("testStringToIntArrayShouldReturnArray")
    void testStringToIntArrayShouldReturnArray(String text, String regex, int[] expected) {
        assertArrayEquals(expected, Part2.stringToIntArray(text, regex));
    }

    @ParameterizedTest
    @MethodSource("testSortCases")
    void testSort(int[] input, int[] expected) {
        Part2.sort(input);
        assertArrayEquals(expected, input);
    }

    @Test
    void testSortShouldThrowIAE() {
        assertThrows(IllegalArgumentException.class, () -> Part2.sort(null));
    }

    static Stream<Arguments> testGenerateCases() {
        return Stream.of(
                Arguments.of(5, 0, 5),
                Arguments.of(50, 0, 7),
                Arguments.of(20, -5, 5),
                Arguments.of(5, -5, 5),
                Arguments.of(1, 0, 5)
        );
    }

    static Stream<Arguments> testStringToIntArrayShouldThrowNFECases() {
        return Stream.of(
                Arguments.of("1. 2 3 4 5 ", " "),
                Arguments.of("1 2 3 4 5.", " "),
                Arguments.of("1 2 3 4 b", "\\s+")
        );
    }

    static Stream<Arguments> testStringToIntArrayShouldReturnArray() {
        return Stream.of(
                Arguments.of("1 2 3 4 5", " ", new int[]{1, 2, 3, 4, 5}),
                Arguments.of(" 1 2 3 4 5 ", " ", new int[]{1, 2, 3, 4, 5}),
                Arguments.of("-4, 1, 7, -12, 3", ", ", new int[]{-4, 1, 7, -12, 3}),
                Arguments.of(String.format(" -4,  %n1,  7, -12, 3  "), ",\\s+", new int[]{-4, 1, 7, -12, 3}),
                Arguments.of(String.format("-5,-4,-3,-2,-1%n"), ",", new int[]{-5, -4, -3, -2, -1})
        );
    }

    static Stream<Arguments> testIntArrayToStringCases() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4, 5}, " ", "1 2 3 4 5"),
                Arguments.of(new int[]{-4, 1, 7, 12, 3}, ", ", "-4, 1, 7, 12, 3"),
                Arguments.of(new int[]{-5, -4, -3, -2, -1}, ",", "-5,-4,-3,-2,-1")
        );
    }

    static Stream<Arguments> testSortCases() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}),
                Arguments.of(new int[]{}, new int[]{}),
                Arguments.of(new int[]{-4, 1, 7, 12, 3}, new int[]{-4, 1, 3, 7, 12}),
                Arguments.of(new int[]{-1, -2, -3, -4, -5}, new int[]{-5, -4, -3, -2, -1})
        );
    }
}
